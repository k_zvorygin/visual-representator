// RunTests.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>

#include "gtest/gtest.h"

int main()
{
    testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}

