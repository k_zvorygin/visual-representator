#pragma once

#include "IView.h"

#include <windows.h>
#include <gdiplus.h>
using namespace Gdiplus;

namespace GuiApplication {
	/*
	 * This class used for drawing
	 */
	class WindowView : public IView
	{
	public:
		WindowView();
		~WindowView();

		void setGraphics(Graphics *hdc);
		virtual void drawAxis();
		virtual void setDimentions(double x_min, double x_max, double y_min, double y_max);
		virtual std::pair<int, int> getDimentions();
		virtual bool drawLine(double x1, double y1, double x2, double y2, Color color);
		virtual bool drawPoint(double x1, double y1, Color color);

	private:
		Graphics * graphics;
		Gdiplus::Color TransformColor(Color col);
		double x_min;
		double x_max;
		double y_min;
		double y_max;
	};

}