#include "WindowsView.h"

#include <windows.h>
#include <objidl.h>
#include <gdiplus.h>
using namespace Gdiplus;

namespace GuiApplication {

    WindowView::WindowView()
    {

    }

    WindowView::~WindowView()
    {
    }

    std::pair<int, int> WindowView::getDimentions()
    {
        return std::pair<int, int>(0,0);
    }

    bool WindowView::drawLine(double x1, double y1, double x2, double y2, Color color )
    {
        Pen      pen(TransformColor(color),2);
        graphics->DrawLine(&pen, (int)(800 * (x1 - x_min) / (x_max - x_min) + 10), (int)(480 * (y1 - y_min) / (y_max - y_min) + 10), (int)(800 * (x2 - x_min) / (x_max - x_min) + 10), (int)(480 * (y2 - y_min) / (y_max - y_min) + 10));
        return true;
    }

    bool WindowView::drawPoint(double x1, double y1, Color color )
    {
        static int r = 0;
        SolidBrush mySolidBrush(TransformColor(color));
        graphics->FillEllipse(&mySolidBrush, (int)(800*(x1-x_min)/(x_max-x_min)+10) , (int)(480 *(y1-y_min)/(y_max - y_min) + 10) , 3, 3);
        //graphics->FillEllipse(&mySolidBrush, (int)(r + 10), (int)(r + 10), 8, 8);
        
        return true;
    }

    Gdiplus::Color WindowView::TransformColor(Color col)
    {
        switch (col)
        {
        case Red: return Gdiplus::Color(255, 0, 0); break;
        case Blue: return Gdiplus::Color(0, 0, 255); break;
        case Green: return Gdiplus::Color(0, 0, 255); break;
        case Black: 
        default:  return Gdiplus::Color(); break;
        }
    }

    void WindowView::setGraphics(Graphics *hdc)
    {
        graphics = hdc;
    }
    void WindowView::drawAxis()
    {
    }

    void WindowView::setDimentions(double x_min, double x_max, double y_min, double y_max)
    {
        //graphics->TranslateTransform(x_max - x_min, y_max - y_min);
        this->x_min = x_min;
        this->x_max = x_max;
        this->y_min = y_min;
        this->y_max = y_max;
    }
}