#pragma once

/* This class represent the state of the one point
*/



struct SamplePoint
{
	double X;
	double Y;
	/* this to point for store */
	int	   X_repr;
	int	   Y_repr;
	/**/
	SamplePoint(double X, double Y, int X_repr, int Y_repr) :
		X(X), Y(Y), X_repr(X_repr), Y_repr(Y_repr) {};
};
