#include "Presenter.h"

#include <algorithm>

namespace GuiApplication {
    
    Presenter::Presenter(IView& view, IMeasureDataSource& Source): view(view),
        DataSource(Source), storage(Source.getDataSets())
    {
        
    }

    Presenter::~Presenter()
    {
    }

    void Presenter::Redraw()
    {
        view.drawAxis();
        view.getDimentions();
        view.setDimentions(x_min, x_max, y_min, y_max);

        std::list<std::pair<double, double>>::iterator ifirst = points.begin();
        std::list<std::pair<double, double>>::iterator inext = points.begin();
        
        for (int i = 0; i < points.size()-1; i++)
        {
            inext++;
            view.drawPoint((*ifirst).first, (*ifirst).second, Blue);
            view.drawLine((*ifirst).first, (*ifirst).second, (*inext).first, (*inext).second, Blue);
            ifirst++;
        }
    }
    void Presenter::ReloadDataFile()
    {
        auto allDatas = storage.getAllDataSets();
        auto ts = allDatas.front().Values;
        allDatas.pop_front();
        auto val = allDatas.front().Values;

        std::list<double>::iterator it1 = ts.begin();
        std::list<double>::iterator it2 = val.begin();

        for (int i = 0; i < val.size(); i++)
        {
            std::pair<double, double> temp(*it1, *it2);
            points.push_back(temp);
            it1++;
            it2++;
        }

        std::pair< std::list<double>::iterator, std::list<double>::iterator> l = std::minmax_element(ts.begin(), ts.end());
        x_min = *(l.first);
        x_max = *l.second;

        std::pair< std::list<double>::iterator, std::list<double>::iterator> p = std::minmax_element(val.begin(), val.end());
        y_min = *(p.first);
        y_max = *(p.second);
    }
}