#pragma once

#include <list>
#include <functional>

namespace GuiApplication {

enum Color {
	Default,
	Red,
	Blue, 
	Black,
	Green,
};

class IView
{
public:
	/* drawing axis for figure */
	virtual void drawAxis() = 0;
	virtual void setDimentions(double x_min, double x_max, double y_min, double y_max) = 0;
	virtual std::pair<int, int> getDimentions() = 0;
	virtual bool drawLine(double x1, double y1, double x2, double y2, Color color) = 0;
	virtual bool drawPoint(double x1, double y1, Color color) = 0;
	virtual ~IView() {};

private:
	std::function<void(int)> temp;
 
};

}
