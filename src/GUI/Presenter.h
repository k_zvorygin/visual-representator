#pragma once

#include <list>

#include "IView.h"
#include "SamplePoint.h"

#include "IMeasureDataSource.h"


namespace GuiApplication {
	/* This class used for calculating and checking how it is looks like*/
	class Presenter
	{
	public:
		Presenter(IView& view, IMeasureDataSource& Source);
		~Presenter();

		void Redraw(); /*!< function that is redraw  */
		void ReloadDataFile();

	private:
		IView& view;
		IMeasureDataSource& DataSource;
		MeasurmentDataStorage& storage;
		std::list<std::pair<double, double>> points;

		double x_min;
		double x_max;
		double y_min;
		double y_max;

	};
}