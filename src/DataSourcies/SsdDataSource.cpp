//#include <boost/log/trivial.hpp>
//#include <thread>
#include <fstream>

#include "SsdDataSource.h"


/*******************************************************
 * 
 */
SsdDataSource::SsdDataSource()
{
}

SsdDataSource::SsdDataSource(string& FileName)
{
	ParseFile(FileName);
}

SsdDataSource::SsdDataSource(istream& TextStream)
{
	Parse(TextStream);
}


void SsdDataSource::ParseFile(string &filename)
{
	string line;
	ifstream inputFile(filename);
	std::string temp("This is first temporary file!!!");

	measurements.setDescription(temp);

	if (inputFile.is_open())
	{
		Parse(inputFile);
		//std::thread t1(Parse, inputFile);
	}
	else
	{
		Error = CanNotOpenFile;
	}
	inputFile.close();
}


void SsdDataSource::Parse(istream& TextStream)
{
	string str;
	list<double> timestamp;
	list<double> value;
	while (getline(TextStream, str))
	{
		/* don't handle string after '#' until new line getted*/
		size_t point = str.find_first_of('#');
		/* if you find '#' symbol in string */
		if (point != string::npos)
		{
			str.erase(point, str.size() - point);
		}
		
		if (!str.empty())
		{
			double ts, val;
			StrHadle(str,ts,val);
			timestamp.push_back(ts);
			value.push_back(val);
		}
	}
	string TimeStampDescr("Timestamp");
	DataSet timestampSet(TimeStampDescr, timestamp);
	measurements.addDataSet(timestampSet);

	string ValueDescr("MeasurementValue");
	DataSet ValueSet(ValueDescr, value);
	measurements.addDataSet(ValueSet);
}

SsdDataSource::~SsdDataSource()
{

}

MeasurmentDataStorage& SsdDataSource::getDataSets()
{
	return measurements;
}


int SsdDataSource::getError()
{
	return Error;
}

bool SsdDataSource::isValid()
{
	return (Error == NoError);
}

void SsdDataSource::StrHadle(string str, double& timestamp, double& value )
{
	timestamp = NAN;
	value = NAN;
	size_t point = str.find_first_of(' ');

	if (point != string::npos)
	{
		try
		{
			timestamp = stod(str.substr(0, str.size() - point));

		}
		catch (std::invalid_argument) {}

		try
		{
			value = stod(str.substr(point, str.size() - point));
		}
		catch (std::invalid_argument) {}
	}
}