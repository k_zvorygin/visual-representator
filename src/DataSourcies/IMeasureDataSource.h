#pragma once

#include <list>
#include <functional>

#include "MeasumentDataStorage.h"
/* 
 * This is interface for connecting data source 
 */
class IMeasureDataSource
{
public:
	/*! take all data points for now it is one dimensional */
	virtual MeasurmentDataStorage& getDataSets() = 0; 
	/*! get errors   */
	virtual int getError() = 0;		
	virtual ~IMeasureDataSource() {};
	std::is_function<void(int)> ret;
private:
};

