#pragma once
#include <iostream>
#include <string>

#include "IMeasureDataSource.h"

using namespace std;

enum SsdDataSourceError {
	NoError = 0,
	CanNotOpenFile,
	WrongFormatOfFile,
};

/* Class for get measurement from the speciefic type of file */
class SsdDataSource  : public IMeasureDataSource
{
public:
	SsdDataSource();
	SsdDataSource(string& FileName);
	SsdDataSource(istream& TextStream);
	~SsdDataSource();
	MeasurmentDataStorage& getDataSets();
	int getError();
	bool isValid(); /*!< file is open and reading compleat */
	void Parse(istream& TextStream);
	void ParseFile(string& filename);
private:
	int Error = 0;
	void StrHadle(string str, double& timestamp, double& value);
	MeasurmentDataStorage measurements;
};

