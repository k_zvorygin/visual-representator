#pragma once

#include <string>
#include <list>

/* all measurement data is stored as one object*/
struct DataSet {
	std::string Measurement;
	std::list<double> Values;
	DataSet(std::string& MeasurementDescr, std::list<double>& val): Values(val), Measurement(MeasurementDescr)
	{

	}
};
/* 
 * This class used for store all measurement data 
 */
class MeasurmentDataStorage
{
public:
	/* default constructor */
	MeasurmentDataStorage();
	/**/
	//MeasurmentDataStorage(std::list<DataSet&> &Sets);
	/**/
	~MeasurmentDataStorage();
	/**/
	void setDescription(std::string &description);
	std::string& getDescription();
	/**/
	void setAdditionalInformation(std::string& additionalInfo);
	std::string& getAddidtionalInformation();
	/* add data set to the big data set */
	void addDataSet(const DataSet data);
	/* get all data information from this measurements*/
	std::list<DataSet>& getAllDataSets();
	/* get specific data set with specific measurement */
	//DataSet& getDataSet(std::string filter);
	
private:
	std::string description;	/*!< This is description of the measurement you can use it to store 
								 *   important and short information  */
	std::string additionalInfo;	/*!< This is additionnal information. Used for storage addinitonal information */
	std::list<DataSet> Measurement; /*!< This is all data information that we took */
};

