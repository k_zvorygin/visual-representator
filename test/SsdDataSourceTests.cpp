#include "gtest/gtest.h"
#include <iostream>
#include <fstream>
#include <chrono>

#include "SsdDataSource.h"

/*lass TestSsdDataSource : public ::testing::Test
{
protected:
	void SetUp()
	{

	}
	void TearDown()
	{

	}
};*/

TEST(TestSsdDataSource, TestingReadingFromFile)
{
	string filename("hello.txt");
	SsdDataSource ssd(filename);
	ASSERT_TRUE(ssd.isValid());
	MeasurmentDataStorage& data = ssd.getDataSets();
	ASSERT_STREQ(data.getDescription().c_str(), "This is first temporary file!!!");
	std::list<DataSet> sets = data.getAllDataSets();
	ASSERT_STREQ(sets.front().Measurement.c_str(), "Timestamp");
	ASSERT_EQ(sets.front().Values.size(), 7);
	ASSERT_DOUBLE_EQ(sets.front().Values.front(), 3.2334289000000e-001);
	sets.pop_front();
	ASSERT_STREQ(sets.front().Measurement.c_str(), "MeasurementValue");
	ASSERT_EQ(sets.front().Values.size(), 7);
	ASSERT_DOUBLE_EQ(sets.front().Values.front(), 1.0000635974181e+007);
}

TEST(TestSsdDataSource, TestingReadingFromIstream)
{
	ifstream file("hello.txt");
	SsdDataSource ssd(file);
	ASSERT_TRUE(ssd.isValid());
	//ASSERT_EQ(ssd.getNumberOfPosints(), 7);
}

TEST(TestSsdDataSource, BigDataFileTest)
{
	//string filename("D:/prj/zvorygin_com/SampleFiles/MillionSamplesWithTail.ssd");
	string filename("D:/prj/zvorygin_com/SampleFiles/big.ssd");
	
	auto t1 = std::chrono::high_resolution_clock::now();
	SsdDataSource ssd(filename);
	auto t2 = std::chrono::high_resolution_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	std::cout <<" Duration:" << duration << "\r\n NumberOfSamples :" << 0 << endl;
}
TEST(StringToDouble, ConvertionStringToDouble)
{
	std::string str("3.2334289000000e-001 1.0000635974181e+007");
	double test = stod(str);
	ASSERT_DOUBLE_EQ(3.2334289000000e-001, test);
}
